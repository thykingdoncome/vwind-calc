# vwind-calc

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

[![Netlify Status](https://api.netlify.com/api/v1/badges/542507ff-f9b7-4ef8-9906-27afd6aa2d41/deploy-status)](https://app.netlify.com/sites/vwind-calc/deploys)